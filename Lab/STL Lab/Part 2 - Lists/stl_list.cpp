// Lab - Standard Template Library - Part 2 - Lists
// Terik, Bragg

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList( list<string>& states )
{
	for (
		list<string>::iterator it = states.begin();
		it != states.end();
		it++
		)
	{
		cout << *it << "\t";
	}
	cout << endl;
}

int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << "1. Push front "
			<< "2. Push back "
			<< "3. Pop front "
			<< "4. Pop back "
			<< "5. Continue " << endl;

		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:			//push front
		{
			cout << "Enter a new state: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;
		}
			
		case 2:			//push back
		{
			cout << "Enter a new state: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;
		}

		case 3:			//pop front
		{
			states.pop_front();
			cout << "Front state removed." << endl;
			break;
		}

		case 4:			//pop back
		{
			states.pop_back();
			cout << "Back state removed." << endl;
			break;
		}

		case 5:			//continue
		{
			done = true;
			break;
		}

		}


	}

	cout << endl << "Original: " << endl;
	DisplayList(states);

	cout << endl << "Reversed: " << endl;
	states.reverse();
	DisplayList(states);

	cout << endl << "Sorted: " << endl;
	states.sort();
	DisplayList(states);


    cin.ignore();
    cin.get();
    return 0;
}
