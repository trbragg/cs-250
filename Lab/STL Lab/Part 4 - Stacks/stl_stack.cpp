// Lab - Standard Template Library - Part 4 - Stacks
// TERIK BRAGG

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> word;

	bool done = false;

	cout << "Enter the next letter, "
		<< "or UNDO to undo, "
		<< "or DONE to finish" << endl;


	while (!done)
	{

		string nextLetter;

		cin >> nextLetter;

		if (nextLetter == "UNDO") 
		{
			cout << word.top() << " removed. " << endl;
			word.pop();
		}
		else if (nextLetter == "DONE")
		{
			done = true;
		}
		else 
		{
			word.push(nextLetter);
		}
	}

	while (!word.empty())
	{
		cout << word.top();
		word.pop();
	}

	cout << endl << "Good bye!";

    cin.ignore();
    cin.get();
    return 0;
}
