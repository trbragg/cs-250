// Lab - Standard Template Library - Part 3 - Queues
// BRAGG, TERIK

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> amount;

	bool done = false;
	while (!done)
	{
		cout << endl << "Transactions queued: " << amount.size() << endl << endl;
		cout << "1. Enqueue Transaction "
			<< "2. Continue "
			<< endl;

		int choice;
		cin >> choice;

		if (choice == 1)
		{
			float num;

			cout << "Enter amount ( + or - ) for next transaction" << endl
				<< ": ";
			cin >> num;
			amount.push(num);
		}
		else
		{
			done = true;
		}

	}

	float balance = 0;

	while (!amount.empty())
	{
		cout << amount.front() << " is added to the account." << endl;
		balance += amount.front();
		amount.pop();
	}


	cout << "Final balance: $" << balance;

    cin.ignore();
    cin.get();
    return 0;
}
