
#include "Tester.hpp"

void Tester::RunTests()
{
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront();
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack();
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_Remove();
	Test_Insert();
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;

	
	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		bool expectedResult = true;
		bool actualResult = testList.IsEmpty();

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		
		for (int i = 0; i < 10; i++)
		{
			testList.PushBack(i);
		}
		testList.ShiftRight(0);

		int expectedFront = 0;
		int actualFront = *testList.GetFront();

		cout << "Expected front: " << expectedFront << endl;
		cout << "Actual front:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

		int expectedBack = 8;
		int actualBack = *testList.GetBack();

		cout << "Expected back: " << expectedBack << endl;
		cout << "Actual back:   " << actualBack << endl;

		if (actualBack == expectedBack)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);
		testList.ShiftLeft(0);

		int expectedFront = 1;
		int actualFront = *testList.GetFront();

		cout << "Expected front: " << expectedFront << endl;
		cout << "Actual front:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		int expectedSize = 1;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		bool expectedResult = true;
		bool actualResult = testList.IsEmpty();

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		bool expectedResult = false;
		bool actualResult = testList.IsEmpty();

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		bool expectedResult = false;
		bool actualResult = testList.IsFull();

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		while (testList.Size() != 100)
		{
			testList.PushBack(1);
		}

		bool expectedResult = true;
		bool actualResult = testList.IsFull();

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);

		int expectedFront = 2;
		int actualFront = *testList.GetFront();

		cout << "Expected front: " << expectedFront << endl;
		cout << "Actual front:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushBack(2);
		testList.PushBack(1);

		int expectedResult = 1;
		int actualResult = *testList.GetBack();

		cout << "Expected back: " << expectedResult << endl;
		cout << "Actual back:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(2);
		testList.PushFront(1);
		testList.PopFront();

		int expectedResult = 2;
		int actualResult = *testList.GetFront();

		cout << "Expected front: " << expectedResult << endl;
		cout << "Actual front:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(2);
		testList.PushFront(1);
		testList.PopBack();

		int expectedResult = 1;
		int actualResult = *testList.GetBack();

		cout << "Expected back: " << expectedResult << endl;
		cout << "Actual back:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(2);
		testList.PushFront(1);
		testList.Clear();

		bool expectedResult = true;
		bool actualResult = testList.IsEmpty();

		cout << "Expected results: " << expectedResult << endl;
		cout << "Actual results:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Get()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		for (int i = 0; i < 10; i++)
		{
			testList.PushBack(i);
		}

		int ind = 4;
		int expectedResult = 4;
		int actualResult = *testList.Get(ind);

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_GetFront()
{
	DrawLine();
	cout << "TEST: Test_GetFront" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(1);
		testList.PushFront(2);

		int expectedFront = 2;
		int actualFront = *testList.GetFront();

		cout << "Expected front: " << expectedFront << endl;
		cout << "Actual front:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_GetBack()
{
	DrawLine();
	cout << "TEST: Test_GetBack" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		testList.PushFront(2);
		testList.PushFront(1);

		int expectedResult = 2;
		int actualResult = *testList.GetBack();

		cout << "Expected back: " << expectedResult << endl;
		cout << "Actual back:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		int item = 1;
		testList.PushBack(2);

		int expectedResult = 0;
		int actualResult = testList.GetCountOf(item);

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		int item = 1;
		int count = 10;
		for (int i = 0; i < count; i++)
		{
			testList.PushBack(item);
		}

		int expectedResult = count;
		int actualResult = testList.GetCountOf(item);


		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl;



	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		int item = 1;
		testList.PushBack(2);

		bool expectedResult = false;
		bool actualResult = testList.Contains(item);

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		int item = 1;
		testList.PushBack(item);

		bool expectedResult = true;
		bool actualResult = testList.Contains(item);


		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Remove()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;

	{   // Test begin
		cout << endl << "Test remove index" << endl;
		List<int> testList;

		int item = 1;
		int count = 12;
		for (int i = 0; i < count; i++)
		{
			testList.PushBack(i%3);
		}

		bool expectedResult = true;
		bool actualResult = testList.RemoveIndex(0);

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		int expectedFront = 1;
		int actualFront = *testList.GetFront();

		cout << "Expected value at index: " << expectedFront << endl;
		cout << "Actual value at index:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		int expectedSize = 11;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

	}   // Test end


	{   // Test begin
		cout << endl << "Test remove item" << endl;
		List<int> testList;

		int item = 1;
		int count = 12;
		for (int i = 0; i < count; i++)
		{
			testList.PushBack(i % 3);
		}

		bool expectedResult = true;
		bool actualResult = testList.RemoveItem(0);

		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		int expectedFront = 1;
		int actualFront = *testList.GetFront();

		cout << "Expected value at index: " << expectedFront << endl;
		cout << "Actual value at index:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		int expectedSize = 8;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

	}   // Test end
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;


	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;

		int item = 1;

		bool expectedResult = false;
		bool actualResult =	testList.Insert(-1, item);


		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		int item = 1;
		testList.PushFront(5);

		bool expectedResult = true;
		bool actualResult = testList.Insert(0, item);

		int expectedSize = 2;
		int actualSize = testList.Size();

		int expectedBack = 5;
		int actualBack = *testList.GetBack();

		int expectedFront = 1;
		int actualFront = *testList.GetFront();



		cout << "Expected result: " << expectedResult << endl;
		cout << "Actual result:   " << actualResult << endl;

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		cout << "Expected Size: " << expectedSize << endl;
		cout << "Actual Size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		cout << "Expected back: " << expectedBack << endl;
		cout << "Actual back:   " << actualBack << endl;

		if (actualBack == expectedBack)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		cout << endl;

		cout << "Expected front: " << expectedFront << endl;
		cout << "Actual front:   " << actualFront << endl;

		if (actualFront == expectedFront)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}