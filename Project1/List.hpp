
#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];

	// functions for interal-workings
	bool ShiftRight(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount) 
		{
			return false;
		}

		for (int i = m_itemCount + 1; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		}
		return true;
	}

	bool ShiftLeft(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = atIndex; i < m_itemCount; i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
		return true;
	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
		Clear();
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount;
	}

	bool    IsEmpty() const
	{
		if (m_itemCount == 0)
		{
			return true;
		}

		return false;
	}

	bool    IsFull() const
	{
		if (m_itemCount == ARRAY_SIZE)
		{
			return true;
		}

		return false;
	}

	bool    PushFront(const T& newItem)
	{
		if (IsFull())
		{
			return false;
		}
		
		ShiftRight(0);
		m_arr[0] = newItem;
		m_itemCount++;
		return true;
	}

	bool    PushBack(const T& newItem)
	{
		if (IsFull())
		{
			return false;
		}

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
	}

	bool    Insert(int atIndex, const T& item)
	{
		if (IsFull())
		{
			return false;
		}

		if (atIndex < 0 || atIndex > Size())
		{
			return false;
		}

		ShiftRight(atIndex);
		m_arr[atIndex] = item;
		m_itemCount++;
		return true;
	}

	bool    PopFront()
	{
		if (IsEmpty())
		{
			return false;
		}

		ShiftLeft(0);
		m_itemCount--;
		return true;
	}

	bool    PopBack()
	{
		if (IsEmpty())
		{
			return false;
		}

		m_itemCount--;
		return true;
	}

	bool    RemoveItem(const T& item)
	{
		if (IsEmpty())
		{
			return false;
		}

		int index = Size();
		while (Contains(item))
		{
			if (m_arr[index] == item)
			{
				RemoveIndex(index);
			}
			index--;
		}

		return true;
	}

	bool    RemoveIndex(int atIndex)
	{
		if (IsEmpty())
		{
			return false;
		}

		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}
		ShiftLeft(atIndex);
		m_itemCount--;
		return true;
	}

	void    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount || IsEmpty())
		{
			return nullptr;
		}
		
		return &m_arr[atIndex];
	}

	T*      GetFront()
	{
		if (IsEmpty())
		{
			return nullptr;
		}
		return &m_arr[0];
	}

	T*      GetBack()
	{
		if (IsEmpty())
		{
			return nullptr;
		}
		return &m_arr[Size() - 1];
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		int counter = 0;
		for (int i = 0; i < Size(); i++)
		{
			if (m_arr[i] == item)
			{
				counter++;
			}
		}
		return counter;
	}

	bool    Contains(const T& item) const
	{
		return (GetCountOf(item) > 0);
	}

	friend class Tester;
};


#endif