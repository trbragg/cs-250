#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	//cout << "First Come First Served (FCFS)" << endl;
	output << "First Come First Served (FCFS)" << endl;


	int cycles = 0;

	//cout << "Processing job #" << jobQueue.Front()->id << "..." << endl;
	output << "Processing job #" << jobQueue.Front()->id << "..." << endl;

	while (jobQueue.Size() > 0)
	{
		jobQueue.Front()->Work(FCFS);

		//cout << "\t" << setw(5) << "cycle: " << setw(5) << cycles << setw(15) << "remaining: "
		//	<< setw(5) << jobQueue.Front()->fcfs_timeRemaining << endl;
		output << "\t" << setw(5) << "cycle: " << setw(5) << cycles << setw(15) << "remaining: "
			<< setw(15) << jobQueue.Front()->fcfs_timeRemaining << endl;

		if (jobQueue.Front()->fcfs_done)
		{
			//cout << "done" << endl << endl;
			output << "done" << endl << endl;

			jobQueue.Front()->SetFinishTime(cycles, FCFS);
			jobQueue.Pop();


			if (jobQueue.Size() > 0)
			{
				//cout << "Processing job #" << jobQueue.Front()->id << "..." << endl;
				output << "Processing job #" << jobQueue.Front()->id << "..." << endl;

			}
		}
		cycles++;
	}


	int timeToComplete = 0;

	//cout << setw(6) << "Job ID" << setw(20) << "Time to Complete" << endl;
	output << setw(6) << "Job ID" << setw(20) << "Time to Complete" << endl;

	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << left << setw(10) << allJobs[i].id
			<< setw(10) << allJobs[i].fcfs_finishTime
			<< endl;
		timeToComplete += allJobs[i].fcfs_finishTime;
	}
	float avgTime = float(timeToComplete) / float(allJobs.size());

	output << endl << endl
		<< "Total time: " << cycles << endl << endl
		<< "Average time: " << avgTime;

	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	//cout << "Round Robin (RR)" << endl;
	output << "Round Robin (RR)" << endl;


	int cycles = 0;
	int timer = 0;

	//cout << "Processing job #" << jobQueue.Front()->id << "..." << endl;
	output << "Processing job #" << jobQueue.Front()->id << "..." << endl;

	while (jobQueue.Size() > 0)
	{
		//timer hits the time per process
		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;

			//cout << endl << endl;
			output << endl << endl
				<< "Processing job #" << jobQueue.Front()->id << endl;

		}

		//Processing the front item
		jobQueue.Front()->Work(RR);

		//cout << "\t" << setw(5) << "cycle: " << setw(5) << cycles << setw(15) << "remaining: "
		//	<< setw(5) << jobQueue.Front()->rr_timeRemaining << endl;
		output << left << "\t" << setw(5) << "cycle: " << setw(5) << cycles << setw(15) << "remaining: "
			<< setw(7) << jobQueue.Front()->rr_timeRemaining 
			<< setw(10) << "interupt: " << setw(5) << jobQueue.Front()->rr_timesInterrupted << endl;



		if (jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, RR);
			jobQueue.Pop();
		}

		cycles++;
		timer++;
	}
	

	int timeToComplete = 0;

	//cout << setw(6) << "Job ID" << setw(20) << "Time to Complete" << endl;
	output << endl << endl
		<< setw(6) << "Job ID" << setw(20) << "Time to Complete" 
		<< setw(20) << "Times interrupted" << endl;

	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		//cout << left << setw(10) << allJobs[i].id
		//	<< setw(10) << allJobs[i].rr_finishTime
		//	<< endl;
		output << left << setw(10) << allJobs[i].id
			<< setw(20) << allJobs[i].rr_finishTime
			<< setw(20) << allJobs[i].rr_timesInterrupted
			<< endl;
		timeToComplete += allJobs[i].rr_finishTime;

	}
	float avgTime = float(timeToComplete) / float(allJobs.size());

	//cout << endl << endl
	//	<< "Total time: " << cycles << endl << endl
	//	<< "Average time: " << avgTime;
	output << endl << endl
		<< "Total time: " << cycles << endl << endl
		<< "Average time: " << avgTime << endl << endl
		<< "Round robin interval: " << timePerProcess;


	output.close();
}

#endif
