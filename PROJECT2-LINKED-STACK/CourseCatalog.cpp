#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );
	cout << left << setw(4) << "#" << setw(10) << "CODE" << setw(30) << "COURSE NAME" <<  endl
		<< setw(6) << "" << "PREREQ" << endl;

	Menu::DrawHorizontalBar(50);

	for (int i = 0; i < m_courses.Size(); i++)
	{
		cout << left << setw(4) << i + 1 << setw(10)
			<< m_courses[i].code << setw(30)
			<< m_courses[i].name << endl;
		if (m_courses[i].prereq != "") 
		{
			cout << setw(6) << "" << m_courses[i].prereq << endl;
		}
		cout << endl;
	}
}

Course CourseCatalog::FindCourse( const string& code )
{
	for (int i = 0; i < m_courses.Size(); i++)
	{ 
		if (m_courses[i].code == code)
		{
			return m_courses[i];
		}
	}
}

void CourseCatalog::ViewPrereqs() noexcept
{
    Menu::Header( "GET PREREQS" );

	string code;
	cout << "Enter class code: ";
	cin >> code;

	LinkedStack<Course> prereqs;

	Course current;
	try
	{
		current = FindCourse(code);
	}
	catch (CourseNotFound ex)
	{
		cout << "Unable to find course " << code << endl;
	}

	prereqs.Push(current);

	while (current.prereq != "")
	{

		try
		{
			//Get the prereq of this course
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound ex)
		{
			cout << ex.what() << endl;
			break;
		}
		// Push the current class into the stack.
		prereqs.Push(current);
	}

	if (prereqs.Size() == 1) 
	{
		cout << "No other courses required! " << endl;
		return;
	}

	//Display the prereqs
	cout << "Courses to take: " << endl;
	int counter = 1;
	while (prereqs.Size() > 0)
	{
		cout << counter << setw(3) << "." << setw(10) << prereqs.Top().code
			<< prereqs.Top().name << endl;

		prereqs.Pop();
		counter++;
	}
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
