#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
		m_itemCount = 0;
    }

    void Push( const T& newData )
    {
		//Allocating memory
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		//Stack is empty
		if (m_ptrFirst == nullptr)
		{
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}
		//Stack has at least one item
		else
		{
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}
		//increment item count
		m_itemCount++;
    }

    T& Top()
    {
		if (m_ptrFirst == nullptr)
		{
			throw out_of_range("Empty stack has no Top()");
		}

		return m_ptrLast->data;
    }

    void Pop()
    {
		//return if there's nothing
		if (m_ptrFirst == nullptr)
		{
			return;
		}

		// only one item on the stack
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}

		else
		{
			//find the second item
			Node<T>* ptrSecondToTop = m_ptrLast->ptrPrev;

			//free the memory for the top item
			delete m_ptrLast;

			//set the new top item
			m_ptrLast = ptrSecondToTop;

			//Set the top item's ptrLast to nullptr
			m_ptrLast->ptrNext = nullptr;
		}

		m_itemCount--;
    }

    int Size()
    {
        return m_itemCount;
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
