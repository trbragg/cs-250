#ifndef _PRIORITY_QUEUE
#define _PRIORITY_QUEUE

#include <iostream>
#include <iomanip>
using namespace std;

#include "Menu.hpp"
#include "Job.hpp"

const int MAX_SIZE = 1000;

class PriorityQueue
{
    public:
    PriorityQueue();
    void Push( Job* task );
    void Pop();
    Job* Front();
    int Size();
    void Display( ostream& stream );

    private:
    void ShiftRight( int atIndex );
    void ShiftLeft( int atIndex );

    Job* m_arr[MAX_SIZE];
    int m_itemCount;
};

PriorityQueue::PriorityQueue()
{
    m_itemCount = 0;
}

void PriorityQueue::Push( Job* task )
{
	if (m_itemCount == MAX_SIZE)
	{
		throw runtime_error("Additional items cannot be added");
		return;
	}

	if (m_itemCount == 0)
	{
		m_arr[0] = task;
		m_itemCount++;
		return;
	}

	for (int i = 0; i < m_itemCount; i++)
	{
		if (m_arr[i]->dueOnDay > task->dueOnDay)
		{
			ShiftRight(i);
			m_arr[i] = task;
			m_itemCount++;
			return;
		}
	}

	m_arr[m_itemCount] = task;
	m_itemCount++;
}

void PriorityQueue::Pop()
{
	ShiftLeft(0);
	m_itemCount--;
}

Job* PriorityQueue::Front()
{
	return m_arr[0];
}

void PriorityQueue::ShiftRight( int atIndex )
{
    for ( int i = m_itemCount; i > atIndex; i-- )
    {
        m_arr[i] = m_arr[i-1];
    }
}

void PriorityQueue::ShiftLeft( int atIndex )
{
    for ( int i = atIndex + 1; i < m_itemCount; i++ )
    {
        m_arr[i-1] = m_arr[i];
    }
}

int PriorityQueue::Size()
{
    return m_itemCount;
}


void PriorityQueue::Display( ostream& stream )
{
    stream << endl;
    stream << left
        << setw( 10 ) << "ID"
        << setw( 10 ) << "DUE ON"
        << setw( 50 ) << "ASSIGNMENT" << endl;

    for ( int i = 0; i < m_itemCount; i++ )
    {
        stream << left
            << setw( 10 ) << m_arr[i]->id
            << setw( 10 ) << m_arr[i]->dueOnDay
            << setw( 50 ) << m_arr[i]->assignment << endl;
    }
}

#endif
